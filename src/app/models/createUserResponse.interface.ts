export interface CreateResponse {
    result?: {
        email?: string;
        user?: {
            uid?: string
        }
    };

    error?: {
        code?: string;
        message?: string;
    };
}
