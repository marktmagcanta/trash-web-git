export interface Enro {
    email: string;
    firstName: string;
    lastName: string;
    userName: string;
    accessType: string;
}
