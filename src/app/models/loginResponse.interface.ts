export interface LoginResponse {
    result?: {
        email?: string;
        user?: object;
    };

    error?: {
        code?: string;
        message?: string;
    };
}
