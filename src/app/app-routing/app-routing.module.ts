import { RedirectComponent } from './../redirect/redirect.component';
import { CreateRouteComponent } from './../pages/create-route/create-route.component';
import { AuthGuard } from './../services/auth.guard';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from '../pages/home/home-page.component';
import { LoginComponent } from '../pages/login/login-page.component';
import { LogoutComponent } from '../components/logout/logout.component';
import { PublishComponent } from '../pages/publish/publish.component';
import { PublishAnnouncementsComponent } from '../pages/publish-announcements/publish-announcements.component';
import { PublishScheduleComponent } from '../pages/publish-schedule/publish-schedule.component';
import { MessagesComponent } from '../pages/messages/messages.component';
import { ReportsComponent } from '../pages/reports/reports.component';
import { RequestsComponent } from '../pages/requests/requests.component';
import { AccountsComponent } from '../pages/accounts/accounts.component';
import { CreateAccountsComponent } from '../pages/create-accounts/create-accounts.component';
import { CreateRoutesComponent } from '../pages/create-routes/create-routes.component';
import { PublishEventsComponent } from '../pages/publish-events/publish-events.component';
import { HelpdeskComponent } from '../pages/helpdesk/helpdesk.component';
import { CreateAnnouncementsComponent } from '../pages/create-announcements/create-announcements.component';
import { ViewInformationDetailsComponent } from '../pages/view-information-details/view-information-details.component';
import { ViewRouteDetailsComponent } from '../pages/view-route-details/view-route-details.component';
import { ViewReportDetailsComponent } from '../pages/view-report-details/view-report-details.component';
import { ViewRequestDetailsComponent } from '../pages/view-request-details/view-request-details.component';
import { MapsComponent } from '../pages/maps/maps.component';
import { MyAccountComponent } from '../pages/my-account/my-account.component';
import { UploadComponent } from '../pages/upload/upload.component';


const routes: Routes = [
  { path: '', redirectTo: 'publish-announcements', pathMatch: 'full'},
  { path: 'home', component: HomeComponent, canActivate: [AuthGuard]},
  { path: 'login', component: LoginComponent},
  { path: 'logout', component: LogoutComponent},
  { path: 'publish', component: PublishComponent, canActivate: [AuthGuard]},
  { path: 'helpdesk', component: HelpdeskComponent , canActivate: [AuthGuard]},
  { path: 'publish-schedule', component: PublishScheduleComponent, canActivate: [AuthGuard]},
  { path: 'publish-events', component: PublishEventsComponent, canActivate: [AuthGuard]},
  { path: 'publish-announcements', component: PublishAnnouncementsComponent, canActivate: [AuthGuard]},
  { path: 'requests', component: RequestsComponent, canActivate: [AuthGuard]},
  { path: 'reports', component: ReportsComponent, canActivate: [AuthGuard]},
  { path: 'messages', component: MessagesComponent, canActivate: [AuthGuard]},
  { path: 'accounts', component: AccountsComponent, canActivate: [AuthGuard]},
  { path: 'create-routes', component: CreateRoutesComponent, canActivate: [AuthGuard]},
  { path: 'create-announcements', component: CreateAnnouncementsComponent, canActivate: [AuthGuard]},
  { path: 'create-accounts', component: CreateAccountsComponent, canActivate: [AuthGuard]},
  { path: 'rep/:id', component: ViewInformationDetailsComponent, canActivate: [AuthGuard]},
  { path: 'min/:id', component: ViewReportDetailsComponent, canActivate: [AuthGuard]},
  { path: 'rou/:id', component: ViewRouteDetailsComponent, canActivate: [AuthGuard]},
  { path: 'req/:id', component: ViewRequestDetailsComponent, canActivate: [AuthGuard]},
  { path: 'map', component: MapsComponent, canActivate: [AuthGuard]},
  { path: 'routes', component: CreateRouteComponent, canActivate: [AuthGuard]},
  { path: 'redirect', component: RedirectComponent, canActivate: [AuthGuard]},
  { path: 'myaccount', component: MyAccountComponent, canActivate: [AuthGuard]},
  { path: 'upload', component: UploadComponent, canActivate: [AuthGuard]}

];

@NgModule({
  imports: [RouterModule.forRoot(routes, {enableTracing: false})],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
