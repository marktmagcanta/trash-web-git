import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { Observable } from 'rxjs/internal/Observable';
import { Router } from '@angular/router';

@Component({
  selector: 'app-requests',
  templateUrl: './requests.component.html',
  styleUrls: ['./requests.component.css']
})
export class RequestsComponent implements OnInit {


  list = [];
  description: string;
  status: string;
  databaseRef$: AngularFireList<any>;
  date;
  timestamp: string;

  userType: string;
  constructor(
    private db: AngularFireDatabase,
    public router: Router,
  ) {
    this.databaseRef$ = this.db.list('REQUEST', ref => ref.orderByKey());
    this.db.list('REQUEST').snapshotChanges().subscribe(route => {
      this.list = [];
      route.forEach(r => {
        const x = r.payload.val();
        x['$key'] = r.key;
        this.list.push(x);
      });
    });
   }

  ngOnInit() {
    this.userType = localStorage.getItem('userType');
  }

  async denied() {
    try {
      const req = await this.databaseRef$.push({
        status: this.status,
    });
    console.log(req, 'Request Denied');
    // this.flashMessagesService.show('New Route Added!', {cssClass:'alert-success', timeout: 4000})
    alert('Request Status is already changed to denied.');
    this.router.navigate(['requests']);

    } catch (error) {
      alert('Sorry, there is something wrong. Please check your internet connection.');
      this.router.navigate(['requests']);
    }
  }

  async accepted() {
    try {
      const req = await this.databaseRef$.push({

      });
    } catch (error) {
      console.log('ew');
    }
  }

  setRef(ref: string) {
    this.databaseRef$ = this.db.list(`REQUEST/${ref}/`);
    console.log(ref);
  }

}

