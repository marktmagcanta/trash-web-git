import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewRouteDetailsComponent } from './view-route-details.component';

describe('ViewRouteDetailsComponent', () => {
  let component: ViewRouteDetailsComponent;
  let fixture: ComponentFixture<ViewRouteDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewRouteDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewRouteDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
