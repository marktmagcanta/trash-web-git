import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase, AngularFireList, AngularFireObject} from 'angularfire2/database';
import { Observable } from 'rxjs/internal/Observable';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-view-route-details',
  templateUrl: './view-route-details.component.html',
  styleUrls: ['./view-route-details.component.css']
})
export class ViewRouteDetailsComponent implements OnInit {

  list = [];

  databaseRef$: AngularFireObject<any>;
  rou: {
    day: string
    type: string
    time: string
    location: string
  };
  id;

  routeLocation: string;
  userType: string;
  constructor(
    private db: AngularFireDatabase,
    public router: Router,
    public route: ActivatedRoute,
  ) {
    this.id = this.route.snapshot.params['id'];
   }

  ngOnInit() {
    this.userType = localStorage.getItem('userType');
    // get ID
    console.log(this.id);

    this.databaseRef$ = this.db.object(`ROUTE/${this.id}`);
   // this.databaseRef$ = this.db.object(`ROUTE_LOCATION/${this.id}`);

    this.databaseRef$.valueChanges().subscribe(data => {
      this.rou = data;

      const location = Object.keys(data.location).map(key => data.location[key]).map(x => x.substr(0, x.length));
      this.routeLocation = location.join();

    });


    // get information
    // this.infoTitle=this.route.snapshot.params['infoTitle'];
    // console.log(this.infoTitle);
  }

}
