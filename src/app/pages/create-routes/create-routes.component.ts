import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
// import { FlashMessagesService } from 'angular2-flash-messages';
import { Router } from '@angular/router';

import { Observable } from 'rxjs/internal/Observable';
import { map } from 'rxjs/operators';
import { FormControl } from '@angular/forms';

import { AmazingTimePickerService } from 'amazing-time-picker';
import { MatFormFieldControl } from '@angular/material';

@Component({
  selector: 'app-create-routes',
  templateUrl: './create-routes.component.html',
  styleUrls: ['./create-routes.component.css']
})


export class CreateRoutesComponent implements OnInit {


  databaseRef$: AngularFireList<any>;
  day: string;
  location: string;
  time: string;
  type: string;
  itemsShowLimit: number;
  dropdownLost: string;
  NewLocation: string;


  // For DROPDOWN
  dropdownList = [];
  selectedLocation = [];
  dropdownSettings = {};
  dayList = [];
  selectedDays = [];
  types = [];


  routeList = [];
  list = [];
  daysChecked = [];
  routeLocations = {};
  typesChecked = [];

  colDays = new FormControl();
  route = new FormControl();
  trashType = new FormControl();
  timeFrom = new FormControl();


  routeId: number;
  routeKEY: string;
  exist = false;

  public time1;
  public time2;

  public time1Disp;
  public time2Disp;


  userType: string;
  constructor(
    private database: AngularFireDatabase,
    // public flashMessagesService: FlashMessagesService,
    public router: Router,
    private db: AngularFireDatabase,
    private atp: AmazingTimePickerService
  ) {
    this.databaseRef$ = database.list('ROUTE/');


  }

  ngOnInit() {
    this.userType = localStorage.getItem('userType');
    this.db.list('ROUTE_LOCATION').snapshotChanges()
    .pipe( map( list =>
        list.map( data => ({
          id: data.key, ...data.payload.val()
        }))
      )
    ).subscribe( routes => {
      this.routeList = routes;
      // console.log("list");
      console.log(this.routeList);

    });

    this.dayList = [
      { id: '1', text: 'Monday'},
      { id: '2', text: 'Tuesday'},
      { id: '3', text: 'Wednesday'},
      { id: '4', text: 'Thursday'},
      { id: '5', text: 'Friday'},
      { id: '6', text: 'Saturday'},
      { id: '7', text: 'Sunday'},
    ];

    this.types = [
      { id: '1', name: 'Biodegradable'},
      { id: '2', name: 'Recyclable'},
      { id: '3', name: 'Residual Non-Biodegradable'},
      { id: '4', name: 'Special Waste'}
    ];




    this.dropdownSettings = {
      singleSelection: false,
      idField: 'id',
      textField: 'text',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 15,
      allowSearchFilter: true
    };
  }

  onAllLocation(items: any) {
    this.selectedLocation.push(items.text);
  }

  onDaySelect(day: any) {
    this.selectedDays.push(day.text);
    console.log(this.selectedDays);
  }

  onSelectDays(days: any) {
    // console.log(days)
  }

  sendData() {
const loc: Array<any> = this.route.value.reduce((obj, item) => (obj[item.id] = item.routeLocationName, obj) , {});
// const day = this.colDays.value.reduce((obj, item) => (obj[item.id] = item.text, obj) , []);
// const day = this.colDays.value;
console.log(this.colDays.value);
this.colDays.value.forEach(element => {
  this.daysChecked.push(element.text);
});

console.log(this.trashType.value);
console.log(loc);
console.log(this.daysChecked);

const days = this.daysChecked.join(', ');
const type = this.trashType.value;


    try {
        const res = this.database.object(`ROUTE/${this.routeKEY}`).set({
          day: days,
          location: loc,
          type: type,
          time: this.time1 + ' - ' + this.time2
        });
          console.log(res, 'Route, Successfully added.');
          alert('Route, Successfully added.');
          this.router.navigate(['publish-schedule']);
    } catch (error) {
        console.log(error, 'Input, invalid. Please re-check form.');
        alert('Data cannot be added. Please re-check form.');
        this.router.navigate(['create-routes']);
    }
  }

  test() {
    console.log(this.route.value);
    console.log(this.colDays.value);

  }

  queryDb() {
    console.log('change');
    const id = 'RN-' + this.routeId;
    console.log(id);

    if (id.indexOf('null', 1) > 0) {
      console.log('null');

    } else {
      this.database.object(`ROUTE/${id}`).valueChanges().subscribe( data => {
        if (data) {
          this.exist = true;
          console.log(this.exist);

        } else {
          console.log(this.exist);

          this.routeKEY = id;
          this.exist = false;
        }
      });
    }

  }


  open(num) {
    const amazingTimePicker = this.atp.open({
        theme: 'material-blue',
    });
    amazingTimePicker.afterClose().subscribe(time => {
        this.tConv24(num, time);
    });
}


tConv24(num, time) {
  const hourEnd = time.indexOf(':');
  const H = +time.substr(0, hourEnd);
  const h = H % 12 || 12;
  const ampm = (H < 12 || H === 24) ? ' AM' : ' PM';

  if (num === 1) {
    this.time1Disp = time;
    this.time1 = h + time.substr(hourEnd, 3) + ampm;
    console.log(this.time1);
  } else {
    this.time2Disp = time;
    this.time2 = h + time.substr(hourEnd, 3) + ampm;
    console.log(this.time2);
  }




}

}
