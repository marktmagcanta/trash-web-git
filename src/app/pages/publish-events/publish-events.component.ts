import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-publish-events',
  templateUrl: './publish-events.component.html',
  styleUrls: ['./publish-events.component.css'],
})
export class PublishEventsComponent implements OnInit {
  userType: string;
  constructor() { }

  ngOnInit() {
    this.userType = localStorage.getItem('userType');
  }

}
