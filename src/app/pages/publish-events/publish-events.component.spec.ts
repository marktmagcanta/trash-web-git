import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublishEventsComponent } from './publish-events.component';

describe('PublishEventsComponent', () => {
  let component: PublishEventsComponent;
  let fixture: ComponentFixture<PublishEventsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublishEventsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublishEventsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
