import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
// import * as firebase from 'firebase';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { AngularFireStorage, AngularFireUploadTask } from 'angularfire2/storage';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';


interface MYFILE {
  title: string;
  fileName: string;
  url: any;
  filePath: string;
}

@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.css']
})


export class UploadComponent implements OnInit {

  type: any[];
  /* MODAL */
  @ViewChild('uploadModal') uploadModal: ElementRef;

  uploadModalRef: NgbModalRef;
  myFile: MYFILE;
  fileName: string;

  file: File;

  private error: boolean;
  private upload: boolean;

  basePath =  'LAWS';
  uploadTask: AngularFireUploadTask;

  // Progress
  private progress: Observable<number>;
  private byteTransferred = 0;
  private totalSize: number;

  // Database
  private listRef$: AngularFireList<any>;

  constructor(private db: AngularFireDatabase,
    private modalService: NgbModal,
    private storage: AngularFireStorage
    ) {
      this.listRef$ = this.db.list('LAWS');
    }

  ngOnInit() {
    this.type = [
      {id: '1', name: 'Law'},
      {id: '2', name: 'Ordinance'}
    ];
  }

  detectFile(event: any) {
    /* const file: any = document.querySelector('#file');

    if (typeof (FileReader) !== 'undefined') {
      const reader = new FileReader;

      reader.onload = (f: any) => {
        this.pdfSrc = f.target.result;
        console.log(this.pdfSrc);

      };

      reader.readAsArrayBuffer(file.files[0]);
    } */

    this.file = event.target.files[0];
    console.log(this.file);
    this.fileName = this.file.name;
    console.log(this.fileName);


    if (this.file.type === 'application/pdf') {
      this.error = false;
      this.upload = true;
    } else {
      this.error = true;
      this.upload = false;
    }
  }


  /* uploadFile(value: any) {
    const storageRef = firebase.storage().ref();

    this.uploadTask = storageRef.child(`${this.basePath}/${this.fileName}`).put(this.file);

    this.uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED,
      // Next Function
      () => {

      // Upload in progress
      // this.progress = (this.uploadTask.snapshot.bytesTransferred / this.uploadTask.snapshot.totalBytes) * 100;
      },

      // Erro Function
      (error) => {
        console.log(error);

      },

      // Completion Function
      () => {
        // this.uploadTask.snapshot.ref.getDownloadURL().then(url => {
        //   console.log(url);
        //   this.myFile.url = url;


        // });



      });
  } */

  upload_file(form: any) {
    const filePath = `${this.basePath}/${this.fileName}`;

    const fileRef = this.storage.ref(filePath);

    this.uploadTask = this.storage.upload(filePath, this.file);

    this.progress = this.uploadTask.percentageChanges();


    this.uploadTask.snapshotChanges().pipe(
      finalize(() =>
        fileRef.getDownloadURL().subscribe( data => {
          this.myUrl(data, form, filePath);

        })

      )
    ).subscribe( data => {

      this.byteTransferred = data.bytesTransferred;
      this.totalSize = data.totalBytes;

    });
  }

  // Pause Upload Function
  pauseUpload() {
    this.uploadTask.pause();
  }

  cancelUpload() {
    this.uploadTask.cancel();
  }

  openUploadModal() {

  this.uploadModalRef = this.modalService.open(this.uploadModal);


  }

  myUrl(url: string, formData: any, filePath: string) {
    console.log(formData);
    // console.log(url);


    console.log(this.myFile);


    try {
      this.listRef$.push({
        fileName: this.fileName,
        path: filePath,
        url: url,
        title: formData.fileTitle,
        type: formData.types
      }).then( result => {
        console.log('Push result', result);
        this.uploadModalRef.close();

      });
    } catch (error) {
      console.log(error);

    }
  }
}
