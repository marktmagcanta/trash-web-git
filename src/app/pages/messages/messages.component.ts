import { AuthenticationService } from './../../services/authentication.service';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { AngularFireDatabase, AngularFireList, AngularFireObject} from 'angularfire2/database';
import { Observable } from 'rxjs/internal/Observable';
import { Router } from '@angular/router';
import { ChatService } from '../../services/chat.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.css']
})
export class MessagesComponent implements OnInit {
  @ViewChild ('content') content: ElementRef;
  list = [];

  databaseRef$: AngularFireList<any>;

  senderName: string;
  sorted: any[];

  myProfile: any;
  chatKEY: string;

  messages: any[];


  chatMessage: string;

  selectedProfile: any;

  headOfficer: boolean;

  userType: string;
  constructor(
    private db: AngularFireDatabase,
    public router: Router,
    database: AngularFireDatabase,
    private chat: ChatService,
    private modalService: NgbModal,
    private auth: AuthenticationService
  ) {
    // this.databaseRef$ = this.db.list('MESSAGES');
    // this.db.list('MESSAGES').snapshotChanges().subscribe(route =>{
    //   this.list = [];
    //   route.forEach(r =>{
    //     var x = r.payload.val();
    //     x["$key"] = r.key;
    //     this.list.push(x);

    //   })
    // })
   }

  ngOnInit() {
    this.userType = localStorage.getItem('userType');
    // console.log(this.userType);

    this.chat.messages().subscribe( data => {
      this.list = data;
      console.log('Chats', this.list);
      this.sorted = this.list.sort((a, b) => {
        return a.date - b.date;
      });
    });

    this.auth.gett().subscribe(data => {
      this.myProfile = data;
      console.error(this.myProfile);

    });


  }


  async openChat(event) {
    console.log(event);
    this.senderName = event.userFromProfile.firstName + ' ' + event.userFromProfile.lastName;
    if (event.userFromId === this.myProfile.userID) {
      console.log('from me');
      this.chatKEY = await event.userToId +  '|' + this.myProfile.userID;
      this.selectedProfile = {
        userID: event.userToId,
        profile: event.userToProfile
      };
    } else {
      console.log('to me');
      this.chatKEY = await event.userFromId + '|' +  this.myProfile.userID;
      this.selectedProfile = {
        userID: event.userFromId,
        profile: event.userFromProfile
      };
    }
    console.log(this.chatKEY);

    const modalRef = this.modalService.open(this.content);

    this.chat.getMessages(this.chatKEY).valueChanges().subscribe( (data: any) => {
      console.log(data);

      this.messages = data.sort( (a, b) => {
        return b.date - a.date;
      });
    });

  }

  // beginDeletingRoute(rep){
  //   if(confirm("Are you sure you want to delete?")){
  //    this.databaseRef$.remove(rep.$key);
  //    alert('Information successfully deleted.')
  //   }
  // }

  async sendMessage() {
    try {
      const messagePayload = {
        userToId: this.selectedProfile.userID,
        userToProfile: {
          firstName: this.selectedProfile.profile.firstName,
          lastName: this.selectedProfile.profile.lastName
        },

        userFromId: this.myProfile.userID,
        userFromProfile: {
          firstName: this.myProfile.userProfile.userName,
          lastName: ''
        },
        content: this.chatMessage,
        date: 0 - Date.now()
      };
      console.log(messagePayload);

      await this.chat.sendMessage(messagePayload, this.chatKEY);
      this.chatMessage = undefined;
    } catch (error) {
      console.log(error);
    }
  }

}
