import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase, AngularFireList, AngularFireObject} from 'angularfire2/database';
import { Observable } from 'rxjs/internal/Observable';
import { Router } from '@angular/router';

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.css']
})
export class ReportsComponent implements OnInit {

  lista = [];

  databaseRef$: AngularFireList<any>;
  date: string;
  location: string;
  sender: string;

  userType: string;

  constructor(
    private db: AngularFireDatabase,
    public router: Router,
    database: AngularFireDatabase
  ) {
    this.databaseRef$ = this.db.list('REPORT');
    this.db.list('REPORT').snapshotChanges().subscribe(route => {
      this.lista = [];
      route.forEach(r => {
        const x = r.payload.val();
        x['$key'] = r.key;
        this.lista.push(x);

      });
    });
   }

  ngOnInit() {
    this.userType = localStorage.getItem('userType');
  }

  beginDeletingRoute(min) {
    if (confirm('Are you sure you want to delete?')) {
     this.databaseRef$.remove(min.$key);
     alert('Report deleted successfully.');
    }
  }
}
