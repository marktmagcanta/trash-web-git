import { Component , Output, EventEmitter, Input, OnInit, ElementRef, ViewChild} from '@angular/core';
import { Subscription, Observable, timer} from 'rxjs';
import {FormControl, FormGroup, FormsModule, Validators} from '@angular/forms';
import {AngularFireDatabase, AngularFireList, AngularFireObject} from 'angularfire2/database';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import { InfoWindowManager } from '@agm/core';

import { map } from 'rxjs/operators';

@Component({
  selector: 'app-maps',
  templateUrl: './maps.component.html',
  styleUrls: ['./maps.component.css']
})

export class MapsComponent implements OnInit {
  title = 'Map';
  lat = 9.3068;
  lng = 123.3054;
  mapReady: any;
  myFunction: any;
  facilityName: any;
  address: any;
  contactNum: any;
  facilityTypes: any[];
  list: AngularFireList<any>;
  latitude: any;
  longitude: any;
  icon: string;
  locs: any[];
  modalName: string;
  responseMessage: string;
  dbObjectRef$: AngularFireObject<any>;
  databaseRef$: AngularFireList<any>;
  facilityData: any;
  previous;
  edit: boolean;
  success = false;

  @ViewChild ('content') content: ElementRef;

  userType: string;
  constructor(private database: AngularFireDatabase, private modalService: NgbModal) {
    this.list = this.database.list('FACILITY_LOCATION/');

  }
   clickedMarker(infowindow) {
    if (this.previous) {
        this.previous.close();
    }
    this.previous = infowindow;
 }
  ngOnInit() {
    this.userType = localStorage.getItem('userType');
    // this._handleMapMouseEvents()
    this.facilityTypes = [
        {id: 1, name: 'Junk Shop'},
        {id: 2, name: 'MRF'},
        {id: 3, name: 'ENRO Office'},
        {id: 4, name: 'Pickup Point'}
    ];

    this.list.snapshotChanges()
    .pipe(
      map(data =>
      data.map( d => ({ key: d.key, ...d.payload.val()}))
    )
    ).subscribe( fal => {
      this.locs = fal;
      console.log(this.locs);

    });

  }

  deleteFacility(facility) {
    this.previous = null;
    if (confirm('Are you sure you want to delete?')) {
     this.list.remove(facility.key);
     alert('Facility successfully deleted.');
    }
  }

  editFacility(facility) {
    this.previous = null;
    this.responseMessage = 'Facility successfully updated';
    this.dbObjectRef$ = this.database.object(`FACILITY_LOCATION/${facility.key}`);
    this.edit =  true;
    this.modalName = 'Edit Facility';
    this.facilityData = facility;
   const modalRef = this.modalService.open(this.content);
  }

  onChoseLocation(event) {
    this.previous = null;
    this.responseMessage = 'Facility successfully added';
    this.edit = false;
    this.modalName = 'Add New Facility';

    this.latitude = event.coords.lat;
    this.longitude = event.coords.lng;

     const modalRef = this.modalService.open(this.content);
  }



  send(value) {
    console.log('form');

    if (value.fal === 'Junk Shop') {
      this.icon = '../assets/markers/junkshopMarker.png';
    } else if (value.fal === 'MRF') {
      this.icon = '../assets/markers/mrfMarker.png';
    } else if (value.fal === 'ENRO Office') {
      this.icon = '../assets/markers/enroMarker.png';
    } else {
      this.icon = '../assets/markers/pickupMarker.png';
    }
      console.log(value.fal);
      try {
        if (this.edit) {
          this.dbObjectRef$.update({
            facilityName: this.facilityData.facilityName,
            address: this.facilityData.address,
            contact: this.facilityData.contact,
            type: value.fal,
            icon: this.icon
          });
        } else {
          this.list.push({
            facilityName: value.falName,
            address: value.falAdd,
            contact: value.falNum,
            type: value.fal,
            coordinates: {
              latitude: this.latitude,
              longitude: this.longitude
            },

            icon: this.icon

          });
        }
      this.onSuccess();

    }  catch (error) {
      console.log(error, 'Input, invalid. Please re-check form.');
      alert('Data cannot be added. Please re-check form.');

    }
  }

  onSuccess() {
    const success = timer(3000);

    this.success = true;

    success.subscribe(() => {
      this.success = false;
    });
  }
}
