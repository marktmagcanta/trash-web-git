import { Component, OnInit, ViewChild } from '@angular/core';
import { AngularFireDatabase, AngularFireList, AngularFireObject} from 'angularfire2/database';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { MatPaginator, MatTableDataSource } from '@angular/material';


@Component({
  selector: 'app-publish-announcements',
  templateUrl: './publish-announcements.component.html',
  styleUrls: ['./publish-announcements.component.css']
})
export class PublishAnnouncementsComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  displayedColumns = ['Date', 'Title', 'Type', 'Actions'];

  list = [];
  infoList =  [];
  dataSource;

  databaseRef$: AngularFireList<any>;
  content: string;
  infoTitle: string;
  infoType: string;

  userType: string;
  constructor(
    private db: AngularFireDatabase,
    public router: Router,
    database: AngularFireDatabase
  ) {
    this.databaseRef$ = this.db.list('INFORMATION', ref => ref.orderByChild('infoTimestamp'));
    /*this.db.list('INFORMATION').snapshotChanges().subscribe(route => {
      this.list = [];
      route.forEach(r => {
        const x = r.payload.val();
        x['$key'] = r.key;
        this.list.push(x);
      });
    }); */


    this.databaseRef$.snapshotChanges().subscribe( list => {
      this.infoList = list.map( data => {
        return { key: data.key, ...data.payload.val()};
      });
      console.log('Info List', this.infoList);
      this.dataSource = new MatTableDataSource(this.infoList);
      this.dataSource.paginator = this.paginator;
    } );


    /* this.informationListRef$.snapshotChanges().subscribe(
      list => {
        this.informationList = list.map( data => {
          return {key: data.key, ...data.payload.val()};
        });
        console.log(this.informationList);

      }
    ); */
   }

  ngOnInit() {
    this.userType = localStorage.getItem('userType');
  }

  beginDeletingRoute(rep) {
    if (confirm('Are you sure you want to delete?')) {
     this.databaseRef$.remove(rep.$key);
     alert('Information successfully deleted.');
    }
  }

  deleteInfo(info: any) {
    console.log(info);
    if (confirm('Are you sure you want to delete?')) {
      try {
        this.db.object(`INFORMATION/${info.key}`).remove();
        alert('Information successfully deleted.');
      } catch (error) {
        alert('Delete failed. Please check your internet connection');
      }
     }
  }

}
