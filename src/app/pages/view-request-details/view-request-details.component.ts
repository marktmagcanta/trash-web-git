import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase, AngularFireList, AngularFireObject} from 'angularfire2/database';
import { Observable } from 'rxjs/internal/Observable';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-view-request-details',
  templateUrl: './view-request-details.component.html',
  styleUrls: ['./view-request-details.component.css']
})
export class ViewRequestDetailsComponent implements OnInit {

  public isButtonVisible = true;

  list = [];

  databaseRef$: AngularFireObject<any>;
  datasender$: AngularFireObject<any>;
  req:
  {
    date: string
    description: string
    requestLocation: string
    image: string
    sentBy: string
    status: string
  };
  id;
  key: string;
  sentBy: {
    firstName: string
    lastName: string
    picture: string
    userRoute: string
  };

  userType: string;

  constructor(
    private db: AngularFireDatabase,
    public router: Router,
    public route: ActivatedRoute,
  ) {
    this.id = this.route.snapshot.params['id'];
   }

  ngOnInit() {
    this.userType = localStorage.getItem('userType');
    // get ID
    console.log(this.id);

    this.databaseRef$ = this.db.object(`REQUEST/${this.id}`);
    // this.databaseRef$.valueChanges().subscribe(data => {
    //   this.req = data

    //   this.datasender$.valueChanges().subscribe(data => this.sentBy = data);
    // })

    this.databaseRef$.snapshotChanges().subscribe( data => {
      this.req = data.payload.val();
      this.key = data.key;
      console.log(this.req);
      console.log(this.key);

      this.datasender$ = this.db.object(`GENERAL_USER/${this.req.sentBy}`);
      this.datasender$.valueChanges().subscribe(dat => this.sentBy = dat);
    });



  }

  denied() {
    if (confirm('DENY this Request?')) {
      this.databaseRef$.update({status: 'Denied'});
      this.isButtonVisible = false;
      alert('Request status has been successfully updated');
    }
    this.isButtonVisible = false;
  }

  accepted() {
    if (confirm('ACCEPT this Request?')) {
      this.databaseRef$.update({status: 'Accepted'});
      this.isButtonVisible = false;
      alert('Request status has been successfully updated');
    }
    this.isButtonVisible = false;
  }

}
