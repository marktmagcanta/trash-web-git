import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
// import { FlashMessagesService } from 'angular2-flash-messages';
import { Router } from '@angular/router';

import { Observable } from 'rxjs/internal/Observable';
import { MatFormFieldControl } from '@angular/material';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-create-announcements',
  templateUrl: './create-announcements.component.html',
  styleUrls: ['./create-announcements.component.css']
})
export class CreateAnnouncementsComponent implements OnInit {

  list: Observable<any>;

  databaseRef$: AngularFireList<any>;
  infoContent: string;
  infoTitle: string;
  infoType: string;
// For DROPDOWN
types = [];

typeInfo = new FormControl();

  userType: string;
  constructor(
    // public flashMessagesService: FlashMessagesService,
    public router: Router,
    private db: AngularFireDatabase
  ) {
    this.databaseRef$ = this.db.list(`INFORMATION/`);
    this.list = this.db.list('ROUTE_LOCATION').valueChanges();
  }

  ngOnInit() {
    this.userType = localStorage.getItem('userType');
    this.types = [
      {id: '1', name: 'Announcement'},
      {id: '2', name: 'Event'},
      {id: '3', name: 'Tip'},
      {id: '4', name: 'Guideline'},
      {id: '5', name: 'Law'}

    ];
  }

  async sendData() {

    try {
      const res = await this.databaseRef$.push({
        infoContent: this.infoContent,
        infoTitle: this.infoTitle,
        infoType: this.typeInfo.value,
        infoTimestamp: 0 - Date.now()
    });
    console.log(res, 'Information, Successfully added.');
    // this.flashMessagesService.show('New Route Added!', {cssClass:'alert-success', timeout: 4000})
    alert('Information, Successfully added.');
    this.router.navigate(['publish-announcements']);

    } catch (error) {
      console.log(error, 'Input, invalid. Please re-check form.');
      // this.flashMessagesService.show('Please fill in the form!', {cssClass:'alert-success', timeout: 4000})
      // this.router.navigate(['publish-schedule']);
      alert('Data cannot be added. Please re-check form.');
      this.router.navigate(['create-announcements']);
    }
  }

  setRef(ref: string) {
    this.databaseRef$ = this.db.list(`INFORMATION/${ref}/`);
    console.log(ref);

  }

}
