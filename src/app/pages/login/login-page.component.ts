import { Enro } from './../../models/account.interface';
import { map } from 'rxjs/operators';
import { AuthenticationService } from './../../services/authentication.service';
import { Component, OnInit, HostBinding } from '@angular/core';
import { Injectable } from '@angular/core';

import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';


import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { AngularFireList, AngularFireDatabase } from 'angularfire2/database';
import { LoginResponse } from 'src/app/models/loginResponse.interface';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css'],

})
export class LoginComponent implements OnInit {

  email: string;
  password: string;

  exist: boolean;

  user: any;

  constructor(
    private database: AngularFireDatabase,
    private auth: AngularFireAuth,
    private router: Router,
    private authentication: AuthenticationService
  ) { }

  ngOnInit() {
  }

  /* async logIn() {
    try {
      const res = await this.auth.auth.signInWithEmailAndPassword(this.email, this.password);

      console.log(res, 'You are now logged-in!');
      alert('You are now logged-in! Welcome to TrashForm!');
      this.router.navigate(['home']);

    } catch (error) {
      console.log(error, 'Sorry, your account is not registered.');
      alert('Sorry, your account is not registered!');
      this.router.navigate(['login']);

    } */




  async logIn() {
    try {
      const res = await this.authentication.checkUser(this.email).toPromise().then((data) => {
        this.user = data;
        console.warn(this.user);

        return (data.length > 0) ?  true :  false;
      });

      if (res) {
        console.log(res);

        this.login();
      } else {
        alert('Sorry, account not registered');
        this.email = null;
        this.password = null;
      }
    } catch (error) {
      console.log(error);

    }




  }

  async login() {
    console.log(this.email.toLowerCase());

    const email: string = this.email.toLowerCase() + '@enro.com';
    const res = await this.authentication.signIn(email, this.password);
    if (!res.error) {
       localStorage.setItem('userType', this.user[0].accessType);
       const re = localStorage.getItem('userType');
       console.log(re);

      alert('You are now logged-in! Welcome to TrashForm!');
      this.router.navigate(['publish-announcements']);
    } else {
      console.error(res.error.code);
      if (res.error.code === 'auth/wrong-password') {
      alert('Incorrect password');
      } else if ( res.error.code === 'auth/network-request-failed') {
        alert('Network error. Please check your internet connection');
        return;
      } else {
        alert(`Unknown error ${res.error.code}`);
      }
      this.email = null;
      this.password = null;
    }

  }



/*   onSubmit(){

    const res = this.databaseRef$

    this.authService.login(this.email, this.password)
    //this.flashMessagesService.show('New Route Added!', {cssClass:'alert-success', timeout: 4000})
    alert('You are now logged in');
    this.router.navigate(['/']);

    } catch (error) {
      //this.flashMessagesService.show('Please fill in the form!', {cssClass:'alert-success', timeout: 4000})
      //this.router.navigate(['publish-schedule']);
      alert('Sorry, account not registered.');
      this.router.navigate(['login']);
    }

  login(email:string, password:string){
    return new Promise((resolve, reject) => {
      this.afAuth.auth.signInWithEmailAndPassword(email, password)
        .then(userData => resolve(userData),
        err => reject(err));
    });
  } */

}
