import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomeComponent implements OnInit {

  userType: string;

  constructor(private auth: AngularFireAuth, private router: Router) {

  }

  ngOnInit() {
    this.userType = localStorage.getItem('userType');

  }

  logOut() {
    this.auth.auth.signOut().then(() => {
      this.router.navigate(['/login']);
    });
  }

}
