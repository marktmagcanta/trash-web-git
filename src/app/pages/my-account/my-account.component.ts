import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFireDatabase, AngularFireObject } from 'angularfire2/database';
import { Observable } from 'rxjs';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { User } from 'firebase';

@Component({
  selector: 'app-my-account',
  templateUrl: './my-account.component.html',
  styleUrls: ['./my-account.component.css']
})
export class MyAccountComponent implements OnInit {

  userType: string;

  userProfileRef: AngularFireObject<any>;
  userProfile: Observable<any>;
  user: User;
  constructor(private router: Router, private db: AngularFireDatabase, private auth: AuthenticationService) {

  this.userProfileRef = this.db.object(`ENRO_OFFICER/${this.auth.getCurUser().uid}`);
  }

  ngOnInit() {
    this.userType = localStorage.getItem('userType');
    if (this.userType === 'Head Officer') {
      this.router.navigate(['/publish-announcements']);
    }

    this.userProfile = this.userProfileRef.valueChanges();

  }

}
