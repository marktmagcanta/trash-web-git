import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublishScheduleComponent } from './publish-schedule.component';

describe('PublishScheduleComponent', () => {
  let component: PublishScheduleComponent;
  let fixture: ComponentFixture<PublishScheduleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublishScheduleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublishScheduleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
