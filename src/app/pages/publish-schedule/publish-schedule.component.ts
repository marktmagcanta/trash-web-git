import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { AngularFireDatabase, AngularFireList} from 'angularfire2/database';
import { Observable } from 'rxjs/internal/Observable';
import { Router, ROUTES } from '@angular/router';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import { MatFormFieldControl } from '@angular/material';
import { map } from 'rxjs/operators';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-publish-schedule',
  templateUrl: './publish-schedule.component.html',
  styleUrls: ['./publish-schedule.component.css']
})
export class PublishScheduleComponent implements OnInit {

  list = [];

  databaseRef$: AngularFireList<any>;
  day: string;
  enroOfficerID: string;
  location: string;
  time: string;
  type: string;

  sort: any[];

  routeLocations: any[];

  btnDisabled = true;

  userType: string;

  routeToEdit: any;
  locsToEdit: any[];

  @ViewChild ('content') content: ElementRef;
  @ViewChild ('edit') edit: ElementRef;
  constructor(
    database: AngularFireDatabase,
    // public flashMessagesService: FlashMessagesService,
    public router: Router,
    private db: AngularFireDatabase,
    private modalService: NgbModal,
    private data: DataService
  ) {
    this.databaseRef$ = this.db.list('ROUTE_LOCATION');
    this.db.list('ROUTE', ref => ref.orderByKey()).snapshotChanges().subscribe(route => {
      this.list = [];
      route.forEach(r => {
        const x = r.payload.val();
        x['$key'] = r.key;
        this.list.push(x);

      });
      this.sort = this.list.sort( (a, b) => {
        const s = +a.$key.slice(3);
        const o = +b.$key.slice(3);

        return  s - o;
      });
    });
   }

  ngOnInit() {
    this.userType = localStorage.getItem('userType');
  }

  addLocation(l) {
    this.getLocations();
    const modalRef = this.modalService.open(this.content);

  }

  beginDeletingRoute(rou) {
    if (confirm('Are you sure you want to delete?')) {
        this.db.list(`ROUTE/${rou.$key}`).remove();
        alert('Route has been deleted.');
    }
  }

  getLocations() {
    this.databaseRef$.snapshotChanges()
      .pipe(map ( locs =>
          locs.map( data => ({key: data.key, ...data.payload.val()}))
        ))
        .subscribe(routeLocs => {
          this.routeLocations = routeLocs;
          // console.log(this.routeLocations);
        })
        ;
  }

  async deleteLocation(location) {
    if (confirm('Are you sure you want to delete this location?')) {
      const res = await this.data.deleteLoc(location.$key);
      console.log(res);
      alert('Location has been deleted.');
    }
  }

  async newLocation() {
    const res = await this.data.addNewLocation(this.location);
  }

  changeBtn(event) {
    if (this.location.trim().length > 0) {
      this.btnDisabled = false;
    } else {
      this.btnDisabled = true;
    }
  }

  editRoute(route) {
    this.routeToEdit = route;
    // console.log(route);
    // console.log(this.routeToEdit.location);
    this.locsToEdit = Object.keys(route.location).map(key => route.location[key]).map(x => x.substr(0, x.length));
    console.log(this.locsToEdit);

   const modalRef = this.modalService.open(this.edit);
   /* this.db.list(`ROUTE/${route.$key}`).snapshotChanges().pipe(
      map( locs =>
        locs.map( data => ({...data.payload})))
    ).subscribe( loc => {

    }); */
  }


  deleteRouteLoc(routeName) {
    try {
      const res = this.db.list(`ROUTE/${this.routeToEdit.$key}/location`, ref => ref.equalTo(routeName) ).remove();
      console.log(res);

    } catch (error) {
      console.log(error);

    }
  }
}
