import { Component, OnInit, NgZone, ViewChild, AfterViewInit } from '@angular/core';
import { MapsAPILoader, AgmMap } from '@agm/core';
import { GoogleMapsAPIWrapper } from '@agm/core/services';


import { FormControl } from '@angular/forms';

import { AmazingTimePickerService } from 'amazing-time-picker';

/* MODAL */
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';

import { map } from 'rxjs/operators';
import { timer } from 'rxjs';
import { Router } from '@angular/router';

declare var google: any;

interface Marker {
  lat: number;
  lng: number;
  label?: string;
  draggable: boolean;
}

interface Location {
  lat: number;
  lng: number;
  viewport?: Object;
  zoom: number;
  address_level_1?: string;
  address_level_2?: string;
  address_country?: string;
  address_zip?: string;
  address_state?: string;
  marker?: Marker;
}
@Component({
  selector: 'app-create-route',
  templateUrl: './create-route.component.html',
  styleUrls: ['./create-route.component.css']
})
export class CreateRouteComponent implements OnInit {

  geocoder: any;

  loading = true;

  title = 'Map';
  lat = 9.3068;
  lng = 123.3054;
  mapReady: any;


  mapMode = 'VIEW';
  mapModeCtrl;

  userType: string;

  previous;
  success: boolean;
  delete: boolean;

  message: string;
  /* MAP MODES */

  createMode = false;
  editMode = false;
  defaultMode = true;

  /* Create Route Variables */
    tempMarkerList = [];
    waypoints = [];
    showDirection = false;
    renderOptions = {
      suppressMarkers: true,
      visible: true
    };
    arrMarkerLen: number;
    showSave = false;
    routeId: number;
    routeKey: string;
    routeIdExist: boolean;

  colDays;
  route;
  trashType;
  timeFrom;
  timeTo;

  dayList = [];
  typeList = [];
  formattedRoute: string;
  modes: any[];

  public time1;
  public time2;

  public time1Disp: string;
  public time2Disp: string;
  /* Create Route Varables END */

  /* VIEW ALL ROUTES */
  allRouteDirections = [];
  routesList = [];
  allRoutes = [];
  allRoutesContainer = [];
  allRoutesRef: AngularFireList<any>;

  routeColors: any[];
  visible =  true;
  /* VIEW ALL ROUTES END */

  /* EDIT ROUTE */
  routeToEdit: any;
  showEdit = false;
  editColDays;
  editDays: string[];
  preDays: string[];
  editType: string[];
  editTrashType;

  public editTime1;
  public editTime2;

  public editTime1Disp;
  public editTime2Disp;
  /*  */


  public defaultLoc: Location = {
    lat: 9.3068,
    lng: 123.3054,
    marker: {
      lat: 9.3068,
      lng: 123.3054,
      draggable: true
    },
    zoom: 14
  };

  private location = {} as Location;

  start = {lat: 9.325275, lng: 123.270879};
  end = {lat: 9.314939, lng: 123.280124};

  @ViewChild(AgmMap) map: AgmMap;

  constructor(public mapsApiLoader: MapsAPILoader,
    private zone: NgZone,
    private wrapper: GoogleMapsAPIWrapper,
    private modalService: NgbModal,
    private database: AngularFireDatabase,
    private atp: AmazingTimePickerService,
    private router: Router) {
      this.mapsApiLoader = mapsApiLoader;
      this.zone = zone;
      this.wrapper = wrapper;

      this.allRoutesRef = this.database.list('ROUTES');
    }

  ngOnInit() {
    this.userType = localStorage.getItem('userType');

    this.mapModeCtrl = new FormControl();
    this.loadMap();
    this.defaultLoc.marker.draggable = true;

    this.dayList = [
      { id: '1', text: 'Monday'},
      { id: '2', text: 'Tuesday'},
      { id: '3', text: 'Wednesday'},
      { id: '4', text: 'Thursday'},
      { id: '5', text: 'Friday'},
      { id: '6', text: 'Saturday'},
      { id: '7', text: 'Sunday'},
    ];

    this.editDays = ['Monday', 'Tuesday', 'Wednesday', 'Thursday'];
    this.editType = ['Biodegradable', 'Recyclable', 'Residual Non-Biodegradable', 'Special Waste'];
    this.typeList = [
      { id: '1', name: 'Biodegradable'},
      { id: '2', name: 'Recyclable'},
      { id: '3', name: 'Residual Non-Biodegradable'},
      { id: '4', name: 'Special Waste'}
    ];

    this.modes = ['VIEW', 'CREATE', 'EDIT'];

    this.routeColors = [
                        '#F7941B',
                        'green',
                        '#0CAAC6',
                        '#F429D3',
                        'yellow',
                        '#AE1382',
                        'black',
                        '#DEB214',
                        '#DA4956',
                        '#229C68',
                        '#6B5179',
                        '#4D5E95',
                        '#C45643',
                        '#BC5422',
                        '#B9617D',
                        '#2AFF02',
                        '#5D6300',
                        '#8C6ED7',
                        '#91949D',
                        '#04B994',
                        '#00B64C',
                        '#60302B',
                        '#A9C766',
                        '#1D1F76',
                        '#AB874C',
                        '#7FBDE2',
                        '#F295BE',
                        '#EAD45A'

                      ];

    this.allRoutesRef.snapshotChanges().pipe(
      map(data => data.map(
        dat => ({ key: dat.key, ...dat.payload.val()})
      ))
    ).subscribe( data => {
      this.allRoutes = [];
      this.getAllRoutes(data);
    });

  }

  getAllRoutes(data: any[]) {
    console.log('MODE', this.mapMode);

    let color = 0;
    data.forEach( routes => {
      console.log(routes);
      let loc = [];
      let waypts = [];
      waypts = routes.waypoints;
      loc = routes.location;
      const len = loc.length - 1;
      console.log(waypts);
      const wayPtsMarker = [];
      /* waypts.forEach( way => {
        wayPtsMarker.push({
          icon: '../../../assets/markers/routeMarkers/RN10M.png'
        });
      } ); */
      this.allRoutes.push({
        type: routes.collectionType,
        time: routes.collectionTime,
        days: routes.collectionDays,
        locs: loc,
        key: routes.key,
        start: loc[0].coords,
        end: loc[len].coords,
        waypoints: waypts,
        visible: false, // this.mapMode === ('EDIT' || 'CREATE') ? false : true,
        renderOptions: {
          preserveViewport: true,
          suppressMarkers: false, // this.mapMode === 'CREATE' ? true : false,
          polylineOptions: {
            strokeColor: this.routeColors[color]
          }
        }, /* ,
        markerOptions: {
          origin: {
            icon: '../../../assets/markers/routeMarkers/RN10M.png'
          },
          destination: {
            icon: '../../../assets/markers/routeMarkers/RN10M.png'
          }
        } */
        index: this.allRoutes.length

      });

      this.allRouteDirections.push({
        type: routes.collectionType,
        time: routes.collectionTime,
        days: routes.collectionDays,
        locs: loc,
        key: routes.key,
        start: loc[0].coords,
        end: loc[len].coords,
        waypoints: waypts,
        visible: false,
        renderOptions: {
          preserveViewport: true,
          suppressMarkers: true,
          polylineOptions: {
            strokeColor: this.routeColors[color]
          }
        },
        index: this.allRouteDirections.length
      });



      /* const tempRoutes: any[] = this.allRoutes;
      tempRoutes.forEach( route => {
        route.renderOptions.suppressMarkers = true;
        route.visible = true;
      });
      this.allRouteDirections = tempRoutes; */
      color = color + 1;
      console.log(color);

    });

    console.log(this.allRoutes);


    const t = timer(8000);

    t.subscribe(() => {
      this.loading = false;
    });
  }



  dropMarker(loc) {
    if (this.createMode) {
      this.visible = true;
      this.tempMarkerList.push({coords: {lat: loc.coords.lat, lng: loc.coords.lng},
        address: this.formattedRoute , index: this.tempMarkerList.length});
      this.findAddressByCoordinates(loc, null);
      console.warn(this.tempMarkerList);
      this.arrMarkerLen = this.tempMarkerList.length;
        if (this.tempMarkerList.length > 2) {
          this.showSave = true;
          this.showDirection = true;
          this.start = this.tempMarkerList[0].coords;
          this.end = this.tempMarkerList[this.arrMarkerLen - 1].coords;
          // this.waypoints = this.tempMarkerList.slice(0 , len - 1);
          this.getWaypoints();
        } else {
          this.showSave = false;
        }
         console.log(this.tempMarkerList);

    }
    if (this.editMode && this.routeToEdit) {
      this.visible = true;
      this.tempMarkerList.push({coords: {lat: loc.coords.lat, lng: loc.coords.lng},
        address: this.formattedRoute , index: this.tempMarkerList.length});
      this.findAddressByCoordinates(loc, null);
      console.warn(this.tempMarkerList);
      this.arrMarkerLen = this.tempMarkerList.length;
        if (this.tempMarkerList.length > 1) {
          this.showDirection = true;
          this.start = this.tempMarkerList[0].coords;
          this.end = this.tempMarkerList[this.arrMarkerLen - 1].coords;
          // this.waypoints = this.tempMarkerList.slice(0 , len - 1);
          this.getWaypoints();
        } else {
          this.showSave = false;
        }
         console.log(this.tempMarkerList);

    }
    // console.error(this.waypoints);
    // console.log(this.tempMarkerList[0].location.address_level_1);
  }

  findAddressByCoordinates(loc, index) {
    this.geocoder.geocode({
      'location': {
        lat: loc.coords.lat,
        lng: loc.coords.lng
      }
    }, (results, status) => {
      console.log(results);
      const address = results[0].formatted_address;
      if (index === null) {
        // console.log(index);
        const len = this.tempMarkerList.length;
        // console.log(len);
        this.tempMarkerList[len - 1].address = address;
      } else  {
        this.tempMarkerList[index].address = address;
      }



      // console.log(this.formattedRoute);
    });

  }

  changeLoc(event, index) {
    // console.log(event);
     console.log(index);
    // console.log(this.tempMarkerList[index].coords);
    this.findAddressByCoordinates(event, index);
    if (index === 0) {
      this.tempMarkerList[0].coords = event.coords;
      // this.tempMarkerList[0].location = this.location;
      this.start = this.tempMarkerList[0].coords;
    } else if (index === this.tempMarkerList.length - 1) {
      this.tempMarkerList[this.tempMarkerList.length - 1].coords = event.coords;
      // this.tempMarkerList[this.arrMarkerLen - 1].location = this.location;
      this.end = this.tempMarkerList[this.tempMarkerList.length - 1].coords;
    } else {
      this.tempMarkerList[index].coords = event.coords;
      this.getWaypoints();
    }
    console.error(this.waypoints);
    // console.error(this.tempMarkerList);

  }

  getWaypoints() {
    // this.waypoints = [];
    if (this.tempMarkerList.length > 2) {
       this.waypoints = [];
      for (let i = 1; i < this.tempMarkerList.length - 1; i++) {
        // console.log(this.tempMarkerList[i].coords);
         this.waypoints.push({
           location: this.tempMarkerList[i].coords,
           stopover: true
         });
      }
    } else {
      return;
    }
  }

  /* change(event) {
    // console.log(event);
    this.waypoints = event.request.waypoints;
    console.log(this.waypoints);
  } */

  async loadMap() {
    await this.mapsApiLoader.load();
    this.geocoder = new google.maps.Geocoder();

  }

  showRouteModal(content) {
    this.modalService.open(content);
  }

  updateOnMap() {
    let full_address: string = this.location.address_level_1 || '';
    if (this.location.address_level_2) { full_address = full_address + ' ' + this.location.address_level_2; }
    if (this.location.address_state) { full_address = full_address + ' ' + this.location.address_state; }
    if (this.location.address_country) { full_address = full_address + ' ' + this.location.address_country; }

    this.findLocation(full_address);
  }

  queryDb(e) {
    console.log('change');
    const id = 'RN-' + this.routeId;
    console.log(id);

    if (id.indexOf('null', 1) > 0) {
      console.log('null');

    } else {
      this.database.object(`ROUTES/${id}`).valueChanges().subscribe( data => {
        if (data) {
          this.routeIdExist = true;
          console.log(this.routeIdExist);

        } else {
          this.routeKey = id;
          this.routeIdExist = false;
          console.log(this.routeIdExist);
        }
      });
    }
  }

  addRoute(value) {
    const arr: any[] = this.colDays.value;
    const x = [];
    arr.forEach( data => {
      x.push(data['text']);
    });
    const days = x.join(' | ');
    const time = this.time1 + ' - ' + this.time2;

   try {
    this.database.object(`ROUTES/${this.routeKey}`).set({
      location: this.tempMarkerList,
      waypoints: this.waypoints,
      collectionDays: days,
      collectionTime: time,
      collectionType: this.trashType.value
    });
    alert('Route has been saved');
    this.router.navigate(['redirect']);
    this.tempMarkerList = [];
    this.trashType = null;
    this.colDays = null;
    this.time1Disp = this.time2Disp = null;
    this.showSave = false;
    this.visible = false;

/*
    this.onSuccess('Route has been added successfully'); */


    /* if (confirm('Saved')) {
      this.router.navigate(['redirect']);
     } else {
       this.router.navigate(['redirect']);
     } */
   } catch (error) {
     console.log(error);

   }

  }

  successSave() {
    alert('Route has been saved');
    this.router.navigate(['redirect']);
  }

  /* findLocation(address) {
    if (!this.geocoder) { this.geocoder = new google.maps.Geocoder(); }
    this.geocoder.geocode({
      'address': address
    }, (results, status) => {
      console.log(results);
      if (status === google.maps.GeocoderStatus.OK) {
                // decompose the result
      } else {
        alert('Sorry, this search produced no results.');
      }
    });
  } */

  findLocation(address) {
    if (!this.geocoder) { this.geocoder = new google.maps.Geocoder(); }
    this.geocoder.geocode({
      'address': address
    }, (results, status) => {
      console.log(results);
      if (status === google.maps.GeocoderStatus.OK) {
        for (let i = 0; i < results[0].address_components.length; i++) {
          const types = results[0].address_components[i].types;

          if (types.indexOf('locality') !== -1) {
            this.location.address_level_2 = results[0].address_components[i].long_name;
          }
          if (types.indexOf('country') !== -1) {
            this.location.address_country = results[0].address_components[i].long_name;
          }
          if (types.indexOf('postal_code') !== -1) {
            this.location.address_zip = results[0].address_components[i].long_name;
          }
          if (types.indexOf('administrative_area_level_1') !== -1) {
            this.location.address_state = results[0].address_components[i].long_name;
          }
        }

        if (results[0].geometry.location) {
          this.location.lat = results[0].geometry.location.lat();
          this.location.lng = results[0].geometry.location.lng();
          this.location.marker.lat = results[0].geometry.location.lat();
          this.location.marker.lng = results[0].geometry.location.lng();
          this.location.marker.draggable = true;
          this.location.viewport = results[0].geometry.viewport;
        }

        this.map.triggerResize();
      } else {
        alert('Sorry, this search produced no results.');
      }
    });
  }

  markerDragEnd(m: any, $event: any) {
    this.location.marker.lat = m.coords.lat;
    this.location.marker.lng = m.coords.lng;
    // this.findAddressByCoordinates();
   }

  decomposeAddressComponents(addressArray) {
    if (addressArray.length === 0) { return false; }
    const address = addressArray[0].address_components;
    for (const element of address) {
      if (element.length === 0 && !element['types']) { continue; }

      if (element['types'].indexOf('street_number') > -1) {
        this.location.address_level_1 = element['long_name'];
        continue;
      }
      if (element['types'].indexOf('route') > -1) {
        this.location.address_level_1 += ', ' + element['long_name'];
        continue;
      }
      if (element['types'].indexOf('locality') > -1) {
        this.location.address_level_2 = element['long_name'];
        continue;
      }
      if (element['types'].indexOf('administrative_area_level_1') > -1) {
        this.location.address_state = element['long_name'];
        continue;
      }
      if (element['types'].indexOf('country') > -1) {
        this.location.address_country = element['long_name'];
        continue;
      }
      if (element['types'].indexOf('postal_code') > -1) {
        this.location.address_zip = element['long_name'];
        continue;
      }
    }

    // console.warn(this.location);
    // console.log(this.formattedRoute);


  }


  /* TIME FORMATTING */
  open(num) {
    const amazingTimePicker = this.atp.open({
        theme: 'material-blue',
    });
    amazingTimePicker.afterClose().subscribe(time => {
        this.tConv24(num, time);
    });
}


tConv24(num, time) {
  const hourEnd = time.indexOf(':');
  const H = +time.substr(0, hourEnd);
  const h = H % 12 || 12;
  const ampm = (H < 12 || H === 24) ? ' AM' : ' PM';

  if (num === 1) {
    this.time1Disp = time;
    console.log(this.time1Disp);

    this.time1 = h + time.substr(hourEnd, 3) + ampm;
    console.log(this.time1);
  } else {
    this.time2Disp = time;
    this.time2 = h + time.substr(hourEnd, 3) + ampm;
    console.log(this.time2);
  }
}

changeMode(mode) {
  if (mode === 'VIEW') {
    console.log('zero');
    this.tempMarkerList = [];
    this.mapMode = 'VIEW';
    this.showEdit = this.editMode = this.createMode = false;
    this.defaultMode = true;
    this.allRoutes.forEach( route => {
      route.visible = true;
      route.renderOptions.suppressMarkers = false;
    });
  }
  if (mode === 'CREATE') {
    console.log('create');

    this.mapMode = 'CREATE';
    this.tempMarkerList = [];
    this.waypoints = [];
    this.visible = false;
    this.colDays = new FormControl();
    this.route = new FormControl();
    this.trashType = new FormControl();
    this.timeFrom = new FormControl();
    this.timeTo = new FormControl();
    this.showEdit = this.defaultMode = this.editMode = false;
    this.createMode = true;
    this.allRoutes.forEach( route => {
      route.visible = false;
    });

    this.allRouteDirections.forEach( route => {
      route.visible = true;
      route.renderOptions.suppressMarkers = true;
    });

    console.log(this.allRouteDirections);


  }
  if (mode === 'EDIT') {
    console.log('two');
    this.mapMode = 'EDIT';
    this.tempMarkerList = [];
    this.visible = false;
    this.colDays = new FormControl();
    this.route = new FormControl();
    this.trashType = new FormControl();
    this.timeFrom = new FormControl();
    this.timeTo = new FormControl();
    this.showSave = this.defaultMode = this.createMode = false;
    this.editMode = true;
    this.allRoutes.forEach( route => {
      route.visible = false;
    });
    this.allRouteDirections.forEach( route => {
      route.visible = true;
      route.renderOptions.suppressMarkers = true;
    });
  }

}

clearRoute() {
  this.visible = false;
  this.showSave = false;
  this.tempMarkerList = [];
  this.start = null;
  this.end = null;
  this.waypoints = [];
  this.previous = null;
  }

chosenRoute() {
  this.setRoute(this.routeToEdit);
  this.previous = null;
}

dropRoute(route: any) {

  console.log('ROUTE', route);
  const locs: Array<any> = route.locs;
  // console.log(locs);
  this.visible = true;
  this.tempMarkerList = [];
  locs.forEach( loc => {
    this.findAddressByCoordinates(loc, null);
    this.tempMarkerList.push({
      coords: {
        lat: loc.coords.lat,
        lng: loc.coords.lng
      },
      address: this.formattedRoute,
      index: this.tempMarkerList.length
    });
    this.arrMarkerLen = this.tempMarkerList.length;
      if (this.tempMarkerList.length > 1) {
        this.showEdit = true;
        this.showDirection = true;
        this.start = this.tempMarkerList[0].coords;
        this.end = this.tempMarkerList[this.arrMarkerLen - 1].coords;
        // this.waypoints = this.tempMarkerList.slice(0 , len - 1);
        this.getWaypoints();
      } else {
        this.showEdit = false;
      }
  });
  console.error(this.tempMarkerList);
  this.setEditVars(route);
}

  setRoute(route: any) {
    this.setEditVars(route);
    const waypoints: any[] = route.waypoints;
    this.tempMarkerList = [];
    const locs: any[] = route.locs;
    this.waypoints = waypoints;
    this.showDirection = this.showEdit = this.visible = true;
    this.start = locs[0].coords;
    this.end = locs[locs.length - 1].coords;

    locs.forEach( loc => {
      this.tempMarkerList.push({
        coords: {
          lat: loc.coords.lat,
          lng: loc.coords.lng
        },
        address: loc.address,
        index: this.tempMarkerList.length
      });
    });

    console.log(this.tempMarkerList);

  }

  setEditVars(route: any) {
    const type: string = route.type;
    const dayString: string = route.days;
    const timeString: string = route.time;
    const days: string[] = dayString.split(' | ');
    const time: string[] = timeString.split(' - ');
    const t1: string = time[0].slice(0, time[0].indexOf(' '));
    const t2: string = time[1].slice(0, time[1].indexOf(' '));
    const t1Out: any[] = t1.split(':');
    const t2Out: any[] = t2.split(':');

    this.time1 = time[0];
    this.time2 = time[1];
    t1Out[0] = this.setTime(t1Out[0], t1);
    t2Out[0] = this.setTime(t2Out[0], t2);

    this.time1Disp = t1Out.join(':');
    this.time2Disp = t2Out.join(':');
    console.log(this.time1Disp);
    console.log(this.time2Disp);

    console.log(days);
    this.preDays = days;

    this.editColDays = new FormControl(days);
    this.editTrashType = new FormControl(type);
  }

  setTime(time: any, timeString: string) {
    time = time * 1;
    if (timeString.search('AM')) {
      console.log('AM');
      if (time === 12) {
        console.log(12);
        return '00';
      } else {
        if (time > 9 ) {
          return time;
        } else {
          time = '0' + time;
          return time;
        }
      }
    } else {
      time = time + 12;
      return time;
    }
  }

  editRoute(value) {
    const arr: any[] = this.editColDays.value;
    const days: string = arr.join(' | ');
    const time = this.time1 + ' - ' + this.time2;

    console.log('UPDATED DAYS', days);
    console.log('UPDATED TIME', time);
    console.log('UPDATED LOCATIONS', this.tempMarkerList);
    console.log('UPDATED WAYPOINTS', this.waypoints);
    console.log('UPDATED TYPE', this.editTrashType.value);

    console.log(this.routeToEdit.key);


    try {
      this.database.object(`ROUTES/${this.routeToEdit.key}`).update({
        location: this.tempMarkerList,
        waypoints: this.waypoints,
        collectionDays: days,
        collectionTime: time,
        collectionType: this.editTrashType.value
      });
      // this.onSuccess('Route has been updated.');
      // this.clearRoute();
      alert('Route has been updated');
      this.router.navigate(['redirect']);
    } catch (error) {
      console.log(error);

    }
  }

/* dropMarker(loc) {
  if (this.createMode) {
    this.visible = true;
    this.tempMarkerList.push({coords: {lat: loc.coords.lat, lng: loc.coords.lng},
      address: this.formattedRoute , index: this.tempMarkerList.length});
    this.findAddressByCoordinates(loc, null);
    console.warn(this.tempMarkerList);
    this.arrMarkerLen = this.tempMarkerList.length;
      if (this.tempMarkerList.length > 1) {
        this.showSave = true;
        this.showDirection = true;
        this.start = this.tempMarkerList[0].coords;
        this.end = this.tempMarkerList[this.arrMarkerLen - 1].coords;
        // this.waypoints = this.tempMarkerList.slice(0 , len - 1);
        this.getWaypoints();
      } else {
        this.showSave = false;
      }
       console.log(this.tempMarkerList);

  }
  // console.error(this.waypoints);
  // console.log(this.tempMarkerList[0].location.address_level_1);
} */


  /* CREATE MARKER INFOWINDOW */
  viewInfo(infowindow) {
    if (this.previous) {
      this.previous.close();
  }
  this.previous = infowindow;
  }

  onSuccess(message: string) {
    this.message = message;
    const success = timer(3000);

    this.success = true;

    success.subscribe(() => {
      this.success = false;
    });
  }

  onDelete(message: string) {
    this.message = message;
    const del = timer(3000);

    this.delete = true;

    del.subscribe(() => {
      this.delete = false;
    });
  }

  deleteRoute(value) {
    try {
      this.database.object(`ROUTES/${this.routeToEdit.key}`).remove();
      this.onDelete('Route has been deleted');
      this.allRouteDirections[this.routeToEdit.index].visible = false;
      this.allRoutes[this.routeToEdit.index].visible = false;
      this.routeToEdit = null;
      this.visible = false;
      this.clearRoute();
      this.showEdit = false;
      alert('Route has been deleted');
      this.router.navigate(['/redirect']);
    } catch (error) {
      console.error(error);

    }
  }
}
