import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewReportDetailsComponent } from './view-report-details.component';

describe('ViewReportDetailsComponent', () => {
  let component: ViewReportDetailsComponent;
  let fixture: ComponentFixture<ViewReportDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewReportDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewReportDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
