import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase, AngularFireList, AngularFireObject} from 'angularfire2/database';
import { Observable } from 'rxjs/internal/Observable';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-view-report-details',
  templateUrl: './view-report-details.component.html',
  styleUrls: ['./view-report-details.component.css']
})
export class ViewReportDetailsComponent implements OnInit {
  lista = [];

  databaseRef$: AngularFireObject<any>;
  datasender$: AngularFireObject<any>;
  min:
  {
    date: string
    description: string
    location: string
    reportImage: string
    sender: string
  };
  id;
  sender:
  {
    firstName: string
    lastName: string
    email: string
    picture: string
    userRoute: string
  };

  userType: string;

  constructor(
    private db: AngularFireDatabase,
    public router: Router,
    public route: ActivatedRoute,
  ) {
    this.id = this.route.snapshot.params['id'];
   }

  ngOnInit() {
    this.userType = localStorage.getItem('userType');
    // get ID
    console.log(this.id);

    this.databaseRef$ = this.db.object(`REPORT/${this.id}`);
    this.databaseRef$.valueChanges().subscribe(data => {
      this.min = data;
      this.datasender$ = this.db.object(`GENERAL_USER/${this.min.sender}`);
      this.datasender$.valueChanges().subscribe(dat => this.sender = dat);
    });
    console.log(this.min);


    console.log(this.sender);
    // get information
    // this.infoTitle=this.route.snapshot.params['infoTitle'];
    // console.log(this.infoTitle);
  }
}
