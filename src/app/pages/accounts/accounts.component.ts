import { AuthenticationService } from './../../services/authentication.service';
import { AccountService } from './../../services/account.service';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { AngularFireDatabase, AngularFireList, AngularFireObject} from 'angularfire2/database';
import { Observable } from 'rxjs/internal/Observable';
import { Router } from '@angular/router';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import { FormControl, NgForm } from '@angular/forms';
import {timer} from 'rxjs';

import * as firebase from 'firebase/app';
interface Temp {
  email: string;
  userName: string;
  firstName: string;
  lastName: string;
  accessType: string;
}
@Component({
  selector: 'app-accounts',
  templateUrl: './accounts.component.html',
  styleUrls: ['./accounts.component.css']
})
export class AccountsComponent implements OnInit {

  /* Username Vars */
  userNameExist: boolean;

  credential: any;

  list = [];
  userType: string;

  databaseRef$: AngularFireList<any>;
  dbObjectRef$: AngularFireObject<any>;
  firstName: string;
  lastName: string;
  userName: string;
  email: string;
  password: string;
  newPassword: string;
  confirmNewPassword: string;
  responseMessage: string;
  accountData: any;
  accessTypes: any[];
  edit: boolean;
  success = false;
  fail = false;
  failMsg: string;
  modalName: string;
  passwordMatch: boolean;

  tempData = {} as Temp;



  @ViewChild ('content') content: ElementRef;
  @ViewChild ('newAccModal') newAccModal: ElementRef;
  @ViewChild ('changePassModal') changePassModal: ElementRef;
  @ViewChild ('reAuthModal') reAuthModal: ElementRef;
  @ViewChild ('reAuthForm') reAuthForm: NgForm;

  constructor(
    private db: AngularFireDatabase,
    public router: Router,
    private modalService: NgbModal,
    private account: AccountService,
    private auth: AuthenticationService
  ) {
    this.databaseRef$ = this.db.list('ENRO_OFFICER');
    this.db.list('ENRO_OFFICER').snapshotChanges().subscribe(route => {
      this.list = [];
      route.forEach(r => {
        const x = r.payload.val();
        x['$key'] = r.key;
        this.list.push(x);
      });
    });
  }

  ngOnInit() {
    this.userType = localStorage.getItem('userType');

    if (this.userType === 'Officer') {
      this.router.navigate(['/home']);
    }

    this.email = '';

    this.accessTypes = [
      {id: '1', name: 'Head Officer'},
      {id: '2', name: 'Officer'}
    ];
  }
addNewAccount() {
this.modalName = 'ADD NEW ACCOUNT';
  this.responseMessage = 'Account successfully added';
  this.edit = false;
  const modalRef = this.modalService.open(this.content);
}

  editAccount(acc) {
    this.modalName = 'EDIT ACCOUNT';
    this.responseMessage = 'Account successfully updated';
    this.dbObjectRef$ = this.db.object(`ENRO_OFFICER/${acc.$key}`);
    this.edit = true;
    this.accountData = acc;

    this.tempData.email = acc.email;
    this.tempData.userName = acc.userName;
    this.tempData.firstName = acc.firstName;
    this.tempData.lastName = acc.lastName;
    this.tempData.accessType = acc.accessType;
    const modalRef = this.modalService.open(this.content);
  }


  beginDeletingRoute(rep) {
    if (confirm('Are you sure you want to delete?')) {
     this.databaseRef$.remove(rep.$key);
    }
  }

  send(value) {
    console.log(value);

    try {
      if (this.edit) {
        console.log(this.edit);
        this.dbObjectRef$.update({
          accessType: value.types,
          firstName: this.tempData.firstName,
          lastName: this.tempData.lastName
        }).then(() => {
          this.onSuccess();
        })
        .catch( error => {
          console.error(error);

        });

      } else {
        console.log(this.edit);
        this.createAccount(value);
      }


    }  catch (error) {
      console.log(error, 'Input, invalid. Please re-check form.');

    }

  }
  onSuccess() {
    const success = timer(3000);

    this.success = true;

    success.subscribe(() => {
      this.success = false;
    });
  }


  async createAccount(value) {
    console.log(value);

    const res = await this.account.createAccount(value.accEmail, value.accPassword);
    if (!res.error) {
      this.dbObjectRef$ = this.db.object(`ENRO_OFFICER/${res.result.user.uid}`);
      try {
        this.dbObjectRef$.set(
          {
          accessType: value.types,
          email: value.accEmail,
          firstName: value.accFirstname,
          lastName: value.accLastname,
          password: value.accPassword,
          userName: value.accUsername
          }

        );
        console.log(res, 'Information, Successfully added.');
        alert('Account, Successfully added.');
      // this.router.navigate(['accounts']);

      } catch (error) {
        alert('Data cannot be added. Please re-check form.');
        this.router.navigate(['create-accounts']);

      }
    } else {
      console.error(res);
      if (res.error.code === 'auth/network-request-failed') {
        const msg = 'Network Error. Please check your internet connection';
        this.onFail(msg);
      } else if ( res.error.code === 'auth/email-already-in-use') {
        const msg = 'The username you provided is already in use';
        this.onFail(msg);
      } else {
        const msg = res.error.code;
        this.onFail(msg);
      }
      // alert('The email address is already in use');
      // this.router.navigate(['create-accounts']);
    }
  }

  onFail(message) {
    this.failMsg = message;
    const fail = timer(3000);

    this.fail = true;

    fail.subscribe(() => {
      this.fail = false;
    });
  }

  openAccModal() {
    const modalRef = this.modalService.open(this.newAccModal);
  }

  query(event: any, status) {
    console.log(event);
    console.log(status);

    this.db.object(`USERID/${event}`).valueChanges().subscribe(data => {
      if (data) {
        this.userNameExist = true;
      } else {
        this.userNameExist = false;
      }
    });

  }

  async addAccount(value) {
    console.log(value);
    const username: string = value.accEmail;
    const email = username.toLowerCase().trim() + '@enro.com';
    console.log(email, username);
    const res = await this.account.createAccount(email, value.accPassword);
    if (!res.error) {
      this.dbObjectRef$ = this.db.object(`ENRO_OFFICER/${res.result.user.uid}`);
      try {
        this.dbObjectRef$.set(
          {
          accessType: value.types,
          email: username.toLowerCase(),
          firstName: value.accFirstName,
          lastName: value.accLastName,
          password: value.accPassword
          }

        );
        console.log(res, 'Information, Successfully added.');
        alert('Account, Successfully added.');
      // this.router.navigate(['accounts']);

      } catch (error) {
        console.log(error);
        alert('Data cannot be added. Please re-check form.');

      }
    } else {
      console.error(res);
      if (res.error.code === 'auth/network-request-failed') {
        const msg = 'Network Error. Please check your internet connection';
        this.onFail(msg);
      } else if ( res.error.code === 'auth/email-already-in-use') {
        const msg = 'The username you provided is already in use';
        this.onFail(msg);
      } else {
        const msg = res.error.code;
        this.onFail(msg);
      }
      // alert('The email address is already in use');
      // this.router.navigate(['create-accounts']);
    }
  }


  openReAuthModal() {
    const reAuthModalRef$ = this.modalService.open(this.reAuthModal);

    reAuthModalRef$.result.catch(error => {
      console.log('dismmised');
      this.email = null;
      this.password = null;
    });
  }

  reAuthenticate(form: any) {
    console.log(form);
    const credential = firebase.auth.EmailAuthProvider.credential(form.accEmail + '@enro.com', form.accPassword);

    console.log(credential);

    const user = this.auth.getCurUser();

    user.reauthenticateAndRetrieveDataWithCredential(credential).then( () => {
      console.log('re auth');
      user.updatePassword(form.newPass).then(() => {
        console.log('password changed');
        alert('Password successfully changed');
      }).catch( err => {
        console.error('failed to change password', err);
        alert('Failed to change password');
      });
    }).catch( err => {
      console.error(err);
      if (err.code === 'auth/wrong-password') {
        alert('Password entered is invalid');
      }
      if (err.code === 'auth/user-mismatch') {
        alert('The supplied credentials do not correspond to the previously signed in user.');
      }
    });
  }

  validatePassword(event) {
    if (event !== this.newPassword) {
      this.passwordMatch = false;
    } else {
      this.passwordMatch = true;
    }

  }

}


