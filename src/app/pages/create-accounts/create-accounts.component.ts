import { AccountService } from './../../services/account.service';
import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase, AngularFireList, AngularFireObject } from 'angularfire2/database';
// import { FlashMessagesService } from 'angular2-flash-messages';
import { Router } from '@angular/router';

import { Observable } from 'rxjs/internal/Observable';
import { MatFormFieldControl } from '@angular/material';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-create-accounts',
  templateUrl: './create-accounts.component.html',
  styleUrls: ['./create-accounts.component.css']
})
export class CreateAccountsComponent implements OnInit {

  constructor(
    private database: AngularFireDatabase,
    // public flashMessagesService: FlashMessagesService,
    public router: Router,
    private account: AccountService
  ) {

  }

  list: Observable<any>;

  databaseRef$: AngularFireObject<any>;
  email: string;
  userName: string;
  password: string;
  firstName: string;
  lastName: string;
  accessType: string;

  // For DROPDOWN
  types = [];

  typeAccess = new FormControl();



  userType: string;
  /* async sendData() {
    try {
      const res = await this.databaseRef$.push({
       userName: this.userName,
       password: this.password,
       firstName: this.firstName,
       lastName: this.lastName,
       accessType: this.accessType
    });
    console.log(res, 'Information, Successfully added.');
    // this.flashMessagesService.show('New Route Added!', {cssClass:'alert-success', timeout: 4000})
    alert('Account, Successfully added.');
    this.router.navigate(['accounts']);

    } catch (error) {
      console.log(error, 'Input, invalid. Please re-check form.');
      // this.flashMessagesService.show('Please fill in the form!', {cssClass:'alert-success', timeout: 4000})
      // this.router.navigate(['publish-schedule']);
      alert('Data cannot be added. Please re-check form.');
      this.router.navigate(['create-accounts']);
    }
  } */


  ngOnInit() {
    this.userType = localStorage.getItem('userType');
    if (this.userType === 'Officer') {
      this.router.navigate(['home']);
    }

    this.email = '';

    this.types = [
      {id: '1', name: 'Head Officer'},
      {id: '2', name: 'Officer'}
    ];


  }
async createAccount() {
    const res = await this.account.createAccount(this.email, this.password);
    if (!res.error) {
      this.databaseRef$ = this.database.object(`ENRO_OFFICER/${res.result.user.uid}`);
      try {
        this.databaseRef$.set(
          {
            email: this.email,
            firstName: this.firstName,
            lastName: this.lastName,
            password: this.password,
            userName: this.userName,
            accessType: this.typeAccess.value
          }

        );
        console.log(res, 'Information, Successfully added.');
        alert('Account, Successfully added.');
      this.router.navigate(['accounts']);

      } catch (error) {
        alert('Data cannot be added. Please re-check form.');
        this.router.navigate(['create-accounts']);

      }
    } else {
      console.error(res);
      alert('The email address is already in use');
      this.router.navigate(['create-accounts']);
    }

  }

}
