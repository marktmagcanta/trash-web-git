import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase, AngularFireList, AngularFireObject} from 'angularfire2/database';
import { Observable } from 'rxjs/internal/Observable';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-view-information-details',
  templateUrl: './view-information-details.component.html',
  styleUrls: ['./view-information-details.component.css']
})
export class ViewInformationDetailsComponent implements OnInit {

  list = [];

  databaseRef$: AngularFireObject<any>;
  rep:
  {
    infoContent: string
    infoTitle: string
    infoType: string
  };
  id;

  userType: string;

  constructor(
    private db: AngularFireDatabase,
    public router: Router,
    public route: ActivatedRoute,
  ) {
    this.id = this.route.snapshot.params['id'];
   }

  ngOnInit() {
    this.userType = localStorage.getItem('userType');

    // get ID
    console.log(this.id);

    this.databaseRef$ = this.db.object(`INFORMATION/${this.id}`);

    this.databaseRef$.valueChanges().subscribe(data => this.rep = data);
    // get information
    // this.infoTitle=this.route.snapshot.params['infoTitle'];
    // console.log(this.infoTitle);
  }


  }
