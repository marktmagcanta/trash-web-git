import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewInformationDetailsComponent } from './view-information-details.component';

describe('ViewInformationDetailsComponent', () => {
  let component: ViewInformationDetailsComponent;
  let fixture: ComponentFixture<ViewInformationDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewInformationDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewInformationDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
