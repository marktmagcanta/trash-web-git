import { HttpClient, HttpClientModule } from '@angular/common/http';
import { ChatService } from './services/chat.service';
import { AuthenticationService } from './services/authentication.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {AgmCoreModule} from '@agm/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';


import { AppComponent } from './app.component';

/** FIREBASE DATABASE */
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { environment } from 'src/environments/environment';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFireStorageModule } from 'angularfire2/storage';

/** Angular Material */
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatFormFieldModule, } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatInputModule, MatToolbarModule, MatListModule, MatButtonModule, MatIconModule, MatProgressBarModule, MatProgressSpinnerModule, MatPaginatorModule, MatTableModule, MatCardModule} from '@angular/material';

/** ROUTER */
import { AppRoutingModule } from './app-routing/app-routing.module';

/** AUTHENTICATION SERVICES AND GUARDS */


 /** FORMS */
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

/** DROPDOWN MENU IMPORTS */
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';

/** TIME PICKER */
import { AmazingTimePickerModule } from 'amazing-time-picker';

/** COMPONENTS */
import { HomeComponent } from './pages/home/home-page.component';
import { LoginComponent } from './pages/login/login-page.component';
import { LogoutComponent } from './components/logout/logout.component';
import { PublishComponent } from './pages/publish/publish.component';
import { PublishAnnouncementsComponent } from './pages/publish-announcements/publish-announcements.component';
import { PublishScheduleComponent } from './pages/publish-schedule/publish-schedule.component';
import { MessagesComponent } from './pages/messages/messages.component';
import { ReportsComponent } from './pages/reports/reports.component';
import { RequestsComponent } from './pages/requests/requests.component';
import { AccountsComponent } from './pages/accounts/accounts.component';
import { CreateAccountsComponent } from './pages/create-accounts/create-accounts.component';
import { CreateRoutesComponent } from './pages/create-routes/create-routes.component';
import { PublishEventsComponent } from './pages/publish-events/publish-events.component';
import { HelpdeskComponent } from './pages/helpdesk/helpdesk.component';
import { CreateAnnouncementsComponent } from './pages/create-announcements/create-announcements.component';
import { ViewInformationDetailsComponent } from './pages/view-information-details/view-information-details.component';
import { ViewRouteDetailsComponent } from './pages/view-route-details/view-route-details.component';
import { ViewReportDetailsComponent } from './pages/view-report-details/view-report-details.component';
import { ViewRequestDetailsComponent } from './pages/view-request-details/view-request-details.component';
import { MapsComponent } from './pages/maps/maps.component';
import { AuthGuard } from './services/auth.guard';
import { DataService } from './services/data.service';
import { CreateRouteComponent } from './pages/create-route/create-route.component';
import { GoogleMapsAPIWrapper } from '@agm/core/services';
import { AgmDirectionModule } from 'agm-direction';
import { RedirectComponent } from './redirect/redirect.component';
import { MyAccountComponent } from './pages/my-account/my-account.component';
import { UploadComponent } from './pages/upload/upload.component';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { FileSizePipe } from './pipes/file-size.pipe';



@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    LogoutComponent,
    PublishComponent,
    PublishAnnouncementsComponent,
    PublishScheduleComponent,
    MessagesComponent,
    ReportsComponent,
    RequestsComponent,
    AccountsComponent,
    CreateAccountsComponent,
    CreateRoutesComponent,
    PublishEventsComponent,
    HelpdeskComponent,
    CreateAnnouncementsComponent,
    ViewInformationDetailsComponent,
    ViewRouteDetailsComponent,
    ViewReportDetailsComponent,
    ViewRequestDetailsComponent,
    MapsComponent,
    CreateRouteComponent,
    RedirectComponent,
    MyAccountComponent,
    UploadComponent,
    FileSizePipe
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    MatFormFieldModule,
    MatSelectModule,
    MatInputModule,
    AppRoutingModule,
    AngularFireAuthModule,
    FormsModule,
    MatToolbarModule,
    MatListModule,
    HttpClientModule,
    AgmCoreModule.forRoot ({apiKey: 'AIzaSyDXXu5Ihtwx4LLhu-eYdx2g-kFZR_IhXhg'}),
    NgMultiSelectDropDownModule.forRoot(),
    NgbModule,
    ReactiveFormsModule,
    AmazingTimePickerModule,
    MatButtonModule,
    MatIconModule,
    AgmDirectionModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatPaginatorModule,
    MatTableModule,
    MatCardModule,
    PdfViewerModule,
    AngularFireStorageModule

  ],
  providers: [AuthenticationService, HttpClient, ChatService, AuthGuard, DataService, GoogleMapsAPIWrapper],
  bootstrap: [AppComponent]
})
export class AppModule { }
