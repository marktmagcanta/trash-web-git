import { LoginResponse } from './../models/loginResponse.interface';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, mergeMap, take, first} from 'rxjs/operators';
import { AngularFireAuth } from 'angularfire2/auth';

@Injectable()
export class AuthenticationService {

    authState = null;
    userRef$: AngularFireList<any>;
    constructor(
        private http: HttpClient,
        private auth: AngularFireAuth,
        private database: AngularFireDatabase) {

        auth.authState.subscribe( state => {
            this.authState = state;
        });

        // this.getUserProfile();
    }

    getCurUser() {
        return this.auth.auth.currentUser;
    }
    /* login(username: string, password: string) {
        return this.http.post<any>(`/users/authenticate`, { username: username, password: password })
            .pipe(map(user => {
                // login successful if there's a jwt token in the response
                if (user && user.token) {
                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                    localStorage.setItem('currentUser', JSON.stringify(user));
                }

                return user;
            }));
    } */

    checkUser(email: string) {
        return this.database.list('ENRO_OFFICER', ref => ref.orderByChild('email').equalTo(email)).snapshotChanges().pipe(
            map( res => {
                return res.map(data => {
                    const val = data.payload.val();

                    return val;
                });
            }
                )
        ).pipe(first());
    }

    async signIn(email: string, password: string) {
        try {
            return <LoginResponse>{
                result: await this.auth.auth.signInWithEmailAndPassword(email, password)
            };
        } catch (err) {
            return <LoginResponse>{
                error:  err
            };
        }
    }

    logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
    }

    getAuthenticatedUser() {
        return this.auth.authState;
    }

    get Authenticated(): boolean {
        return this.authState !== null;
    }

    get currentUserObservable(): any {
        return this.auth.authState;
      }



    getUserProfile() {
        return this.getAuthenticatedUser()
            .pipe( map(user => user.uid))
            .pipe( mergeMap( uid => this.database.object(`/ENRO_OFFICER/${uid}`).snapshotChanges()
                .pipe(map(data => {
                    console.log(data);
                    console.log(data.key);
                    console.log(data.payload.val());
                }))
            )

            );

    }

    get() {
        this.getAuthenticatedUser()
            .pipe( map(user => user.uid))
            .pipe( map(uid =>  this.database.object(`/ENRO_OFFICER/${uid}`).snapshotChanges().subscribe(
                prof => {
                    const a = prof.key;
                    const b = prof.payload.val();
                    console.log(a);
                    console.log(b);
                }
            ) ) );

    }

    /* gett() {

        return this.getAuthenticatedUser()
            .pipe( map(user => user.uid))
            .pipe( mergeMap(uid => {
                console.log('UID', uid);
                return this.database.object(`ENRO_OFFICER/${uid}`).snapshotChanges().pipe(take(1))
                .pipe(
                    map(data => {

                        const userID = data.key;
                        const userProfile = data.payload.val();
                        console.log(userID);

                        return {userID, userProfile};
                    })
                );
            }));
    } */

    gett() {

        return this.getAuthenticatedUser()
            .pipe( map(user => user.uid))
            .pipe( mergeMap(uid => {
                console.log('UID', uid);
                return this.database.object(`ENRO_OFFICER/${uid}`).snapshotChanges().pipe(take(1))
                .pipe(
                    map(data => {

                        const userID = data.key;
                        const userProfile = data.payload.val();
                        console.log(userID);

                        return {userID, userProfile};
                    })
                );
            }));
    }

    gets() {
        return this.getAuthenticatedUser().pipe(
            map(
                user => {
                    this.database.object(`ENRO_OFFICER/${user.uid}`).snapshotChanges()
                        .pipe(
                            map( data => {
                                const a = data.key;
                                const b = data.payload.val();

                                return {a, b};
                            })
                        );
                }
            )
        );
    }

    getss() {
        return this.getAuthenticatedUser().pipe(map(auth => auth.uid))
        .pipe(
            mergeMap(uid => this.database.object(`ENRO_OFFICER/${uid}`).snapshotChanges())
        )
        .pipe(map(id => {
            const a = id.key;
                                const b = id.payload.val();
                                console.log(a);
                                console.log(b);
                                return {a, b};
        })), (...value) => {
            console.log(value);

            return value;
        };
    }



  /*   getAuthenticatedUserProfile() {
        return this.authService.getAuthenticatedUser()
        .map(user => user.uid)
        .mergeMap(authId => this.database.object(`/GENERAL_USER/${authId}`).snapshotChanges().take(1));
      } */



}
