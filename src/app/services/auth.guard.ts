import { AuthenticationService } from './authentication.service';
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { take, map, tap } from 'rxjs/operators';



@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private auth: AuthenticationService, private router: Router) {

  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
      if (this.auth.Authenticated) {
        return true;
      } else {

      return this.auth.currentUserObservable.pipe(
        map(user => !!user), tap(loggedIn => {
          if (!loggedIn) {
            console.log('denied');
            this.router.navigate(['/login']);
          }
        })
      );
        }

  }
}
