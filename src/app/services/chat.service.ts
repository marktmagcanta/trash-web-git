import { AngularFireDatabase } from 'angularfire2/database';
import { AuthenticationService } from './authentication.service';
import { Injectable  } from '@angular/core';
import { map, mergeMap, first} from 'rxjs/operators';
import { Observable, forkJoin } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class ChatService {

  constructor(private auth: AuthenticationService, private database: AngularFireDatabase) { }


/*   getLastMessages(){
    return this.auth.getAuthenticatedUser().pipe()
    .map(auth => auth.uid)
    .mergeMap(authId => this.database.list(`/USER_MESSAGES/${authId}`).snapshotChanges())
    .mergeMap(messageIds => {
      //console.log(messageIds);
        return Observable.forkJoin(messageIds.map( message => {
          //return this.database.object(`/MESSAGES/${message.key}`).valueChanges().first()
          return this.database.object(`/MESSAGES/${message.payload.val().chatID}/${message.payload.val().key}`).valueChanges().first()
        }),
        (...values) => {
          //console.error(values)
          return values
        }
        )
    })
  } */


  messages() {
    /* this.auth.getAuthenticatedUser().subscribe( data => {
      console.log(data.uid);
    }); */
    // tslint:disable-next-line:max-line-length
    return this.auth.getAuthenticatedUser().pipe( map( aut => aut.uid)).pipe( mergeMap(authid => this.database.list(`/USER_MESSAGES/${authid}`).snapshotChanges()))
    .pipe( mergeMap( messageIds => {
       console.log('asdf');
      return forkJoin(messageIds.map( message => {
         console.log('mess');
        // tslint:disable-next-line:max-line-length
        return this.database.object(`/MESSAGES/${message.payload.val()['chatID']}/${message.payload.val()['key']}`).valueChanges().pipe(first());
      }),
      (...value) => {
        console.log(value);
        return value;
      }
      );
    }));
  }

  getMessages(chatKEY) {
    return this.database.list(`MESSAGES/${chatKEY}`, ref => ref.orderByChild('date'));
  }

  async sendMessage(messagePayload, chatKEY) {
    await this.database.list(`/MESSAGES/${chatKEY}`).push(messagePayload);
  }

/*   async mes() {
    return this.auth.getAuthenticatedUser().pipe(map(auth => auth.uid)).pipe(
      map(authid => {
        console.log('asfdsaf');
        console.log(authid);
      })
    );
  } */
}
