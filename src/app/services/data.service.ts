import { AngularFireDatabase, AngularFireObject, PathReference } from 'angularfire2/database';
import { Injectable } from '@angular/core';
import { RouteLocResponse } from '../models/routeLoc.interface';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  pushId: string;
  constructor(private database: AngularFireDatabase) {
    this.pushId = this.database.createPushId();
   }

  deleteLoc(key) {
    try {
      const res = this.database.object(`ROUTE_LOCATION/${key}`).remove();
      console.log(res);
      return true;
    } catch (error) {
      console.error(error);
      return error;
    }
  }

  deleteRoute(key) {
    try {
      const res = this.database.object(`ROUTE/${key}`).remove();
      console.log(res);
      return true;
    } catch (error) {
      console.error(error);
      return error;
    }
  }


  async addNewLocation(newLocationName) {
    try {
      const res = await this.database.list(`ROUTE_LOCATION`).push(this.pushId).set({routeLocationName: newLocationName});
      return res;
    } catch (error) {
      console.error(error);
      return error;
    }
  }
}
