import { AngularFireAuth } from 'angularfire2/auth';
import { Injectable } from '@angular/core';
import { CreateResponse } from '../models/createUserResponse.interface';

@Injectable({
  providedIn: 'root'
})
export class AccountService {

  constructor(private auth: AngularFireAuth) { }


  async createAccount(email, password) {
    try {
      return <CreateResponse> {
        result: await this.auth.auth.createUserWithEmailAndPassword(email, password)
      };
    } catch (err) {
      return <CreateResponse>{
        error: err
      };
    }
  }
}
