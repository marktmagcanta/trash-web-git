// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyDXXu5Ihtwx4LLhu-eYdx2g-kFZR_IhXhg',
    authDomain: 'trashform-v1.firebaseapp.com',
    databaseURL: 'https://trashform-v1.firebaseio.com',
    projectId: 'trashform-v1',
    storageBucket: 'trashform-v1.appspot.com',
    messagingSenderId: '685005862168'

  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
